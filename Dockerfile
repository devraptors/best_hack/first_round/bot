#
#   Maven stage
#
FROM maven:3.6.3-openjdk-15 AS maven
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package assembly:single
#
#   Build stage
#
FROM openjdk:15
COPY --from=maven /home/app/target/Telegram_Bot-1.0-jar-with-dependencies.jar /home/app/target/Telegram_Bot.jar
WORKDIR /home/app/target
CMD ["java","-jar","Telegram_Bot.jar"]