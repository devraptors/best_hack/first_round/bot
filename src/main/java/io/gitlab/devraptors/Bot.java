package io.gitlab.devraptors;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.*;
import java.util.logging.Logger;


public  class Bot extends TelegramLongPollingBot {
    private static final String TOKEN = System.getenv("BOT_TOKEN");
    private static final String USERNAME = System.getenv("BOT_NAME");
    static Logger logger = Logger.getLogger(Start.class.getName());
    String[] country = {"Россия", "Германия", "Франция", "Великобритания", "Турция", "Австрия", "Испания", "Готово"};

    public Bot(DefaultBotOptions options){
        super(options);
    }
    public String getBotToken(){
        return TOKEN;
    }
    public String getBotUsername(){
        return USERNAME;
    }
    private ReplyKeyboardMarkup createKeyboard(String[] list)
    {

        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        for (String button: list){
            KeyboardRow row = new KeyboardRow();
            row.add(button);
            keyboard.add(row);
        }

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }
    public void onUpdateReceived(Update updates){
        Message message = updates.getMessage();
        Database database = Database.getInstance();
        logger.info(message.getText());
        if(message != null && message.hasText()){
            String text = message.getText();
            long userId = message.getChatId();
            User currentUser = database.getUserById(userId);

            try {
                if(currentUser.getState() == State.AWAIT_TO_CITIES)
                {
                    if(text.equals("Готово"))
                    {
                        currentUser.setState(State.NEUTRAL);
                        SendMessage sendMessage = new SendMessage(userId, "Чтобы получить информацию об авиабилетах, нажми Обновить");
                        String[] update = {"Обновить"};
                        sendMessage.setReplyMarkup(createKeyboard(update));
                        execute(sendMessage);
                    }
                    else
                    {
                        currentUser.addToCity(text);
                        SendMessage sendMessage = new SendMessage(userId, "Выбери место прибытия.");
                        sendMessage.setReplyMarkup(createKeyboard(country));
                        execute(sendMessage);
                    }
                } else if(currentUser.getState() == State.AWAIT_FROM_CITY)
                {
                    currentUser.setFromCity(text);
                    currentUser.setState(State.AWAIT_TO_CITIES);

                    SendMessage sendMessage = new SendMessage(userId, "Выбери место прибытия.");

                    sendMessage.setReplyMarkup(createKeyboard(country));
                    execute(sendMessage);
                } else if (text.equals("/start")){
                    currentUser.setState(State.AWAIT_FROM_CITY);
                    String fullName = message.getFrom().getFirstName();
                    SendMessage sendMessage = new SendMessage(userId, "Добро пожаловать " + fullName + " - выбери место, где ты живешь.");
                    sendMessage.setReplyMarkup(createKeyboard(country));
                    execute(sendMessage);
                } else if (text.equals("Обновить")){
                    SendMessage sendMessage = new SendMessage(userId, "билетов нет. пака.");
                    execute(sendMessage);
                }
            }
            catch (TelegramApiException e){
                e.printStackTrace();
            }
        }
    }

}
