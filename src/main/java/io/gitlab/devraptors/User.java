package io.gitlab.devraptors;

import java.util.*;

enum State{
    AWAIT_FROM_CITY,
    AWAIT_TO_CITIES,
    NEUTRAL
}
public class User {
    long userId;
    String fromCity;
    List<String> toCities;
    State state;

    public User(long userId){
        this.userId = userId;
        this.toCities = new ArrayList<String>();
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public void addToCity(String city) {
        for (String c: this.toCities) {
            if (c.equals(city)){
                return;
            }
        }

        this.toCities.add(city);
    }

    public void setState(State state) {
        this.state = state;
    }

    public long getUserId() {
        return userId;
    }

    public String getFromCity() {
        return fromCity;
    }

    public List<String> getToCities(){
        return toCities;
    }

    public State getState() {
        return state;
    }
}
