package io.gitlab.devraptors;

import java.util.ArrayList;
import java.util.List;

public class Database {
    List<User> users;
    private static Database instance;

    public Database() {
        this.users = new ArrayList<User>();
    };

    private User addUser(User user)
    {
        users.add(user);
        return user;
    }

    public static Database getInstance()
    {
        if(instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public User getUserById(long userId)
    {
        for (User user: users) {
            if(user.getUserId() == userId)
                return user;
        }

        return this.addUser(new User(userId));
    }
}
